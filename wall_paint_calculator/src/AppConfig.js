const appConfig = {
    WALLS_AMMOUNT: 4,
    WALL_MIN_AREA: 1,
    WALL_MAX_AREA: 15,
    DOOR: {
        WIDTH: 0.8,
        HEIGHT: 1.9,
    },
    WINDOW: {
        WIDTH: 2.0,
        HEIGHT: 1.2
    },
    START_INPUTS: {
        WALL_HEIGHT: 2.2,
        WALL_WIDTH: 5,
        DOORS_AMOUNT: 0,
        WINDOWS_AMOUNT: 0
    }
}

export default appConfig